function runBuildpack() {
    mkdir -p /tmp/.logs/
    mkdir -p ./reports
    echo "[$(date)]" >> /tmp/.logs/.buildlogs
    echo "Use Kaniko build or Cloud Native Buildpacks and build the source code as an OCI image" | tee -a /tmp/.logs/.buildlogs
    
    idevops_image=$(basename $PWD)
    echo "image name: $idevops_image" | tee -a /tmp/.logs/.buildlogs
    DOCKERFILE=./Dockerfile
    if [ -f "$DOCKERFILE" ]; then
        echo "Using Kaniko build" 2>&1 | tee -a /tmp/.logs/.buildlogs
        docker run -it --rm -v ${PWD}:/workspace    \
            -v /tmp:/out    \
            gcr.io/kaniko-project/executor:latest --no-push --tarPath="/out/${idevops_image}.tar" --destination=/dev/null 2>&1 | tee -a /tmp/.logs/.buildlogs
        docker image import "/tmp/${idevops_image}.tar" "${idevops_image}"
    else
        echo "Using Cloud Native Buildpacks" 2>&1 | tee -a /tmp/.logs/.buildlogs
        docker run --rm -v /var/run/docker.sock:/var/run/docker.sock    \
            -v $PWD:/workspace  \
            -w /workspace   \
            buildpacksio/pack build "${idevops_image}" --builder heroku/buildpacks:18 2>&1 | tee -a /tmp/.logs/.buildlogs
    fi
}
runBuildpack